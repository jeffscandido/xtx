/* -*- c++ -*- */
/* 
 * Copyright 2020 Jefferson da Silva Candido.
 * 
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_XTX_HIERARCHICAL_DIVISOR_IMPL_H
#define INCLUDED_XTX_HIERARCHICAL_DIVISOR_IMPL_H

#include <xtx/hierarchical_divisor.h>

namespace gr {
  namespace xtx {

    class hierarchical_divisor_impl : public hierarchical_divisor
    {
     private:
      // Nothing to declare in this block.

     public:
      hierarchical_divisor_impl(int mode, float cp, int segments_A, int mod_scheme_A, float conv_code_A, int segments_B, int mod_scheme_B, float conv_code_B, int segments_C, int mod_scheme_C, float conv_code_C);
      ~hierarchical_divisor_impl();

      // Where all the action really happens
      void forecast (int noutput_items, gr_vector_int &ninput_items_required);

      int general_work(int noutput_items,
           gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
    };

  } // namespace xtx
} // namespace gr

#endif /* INCLUDED_XTX_HIERARCHICAL_DIVISOR_IMPL_H */

